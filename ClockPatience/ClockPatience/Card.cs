﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClockPatience
{
    class Card
    {

      
        private int value;
 
        
        public Card(int cardValue)
        {
            value = cardValue;
        }


        public int Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;//value (key word) is a parameter
            }
        }

    

        public void Print()
        {
            Console.WriteLine("Card value: " + this.Value);
        }

    }
 
}
