﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ClockPatience
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WindowWidth = 80;
            Console.Title = "Alex Scicluna - C# Clock Paitence";
            Console.WriteLine("Clock Paitence Game/Simulation!\n");

            int gameLoopCounter = 0;
            int gameWinCounter = 0;
            for (int gameLoop = 0; gameLoop < 100; gameLoop++)
            {
                gameLoopCounter++;

                List<Card> deck = new List<Card>();
            
                //Console.WriteLine(deck.Count);

                //1. initialise the deck of 52 cards.

          
                for (int outerIndex = 0; outerIndex < 13; outerIndex++)
                {
                    for (int innerIndex = 0; innerIndex < 4; innerIndex++)
                    {
                        //Uses the currentCard_Setup indexer to add a value
                        //This will be 4 of each number ranging from 1-13
                        //Then increment the current card
                        deck.Add(new Card(outerIndex + 1));
                    }
                }
            
 
                //2. shuffle the deck
                //Console.WriteLine("[Shuffle the Deck]");
                ShuffleDeck(deck);




                //3. create the board
                //4.  loop 52 times, take the top card of the shuffled deck and add it to board[index] (start at 0)
                //increment index until it gets to 12 (reset it back to zero)
                //board is an array of 13 lists of Cards
                List<Card>[] board = new List<Card>[13];


                for(int i = 0; i < 13; i++)
                {
                    //loop over all of them and create new lists
                    //assign the pointer to an actual list
                    board[i] = new List<Card>();
                }
               

                int currentCardIndex = 0;

                for (int outerIndex = 0; outerIndex < 4; outerIndex++)
                {

                    for (int innerIndex = 0; innerIndex < 13; innerIndex++)
                    {
                        //change after the shuffle
                        //Board position -> Current Position
                        //deck[currentCardIndex].CurrentPosition = innerIndex;//0-12

                        //add all cards to your game board 
                        //board is innerIndex 0-12
                        //deck access via a global currentCardIndex 0-51

                        //Add a card to the board while it is less then 52
                        if (currentCardIndex != deck.Count)
                        {
                            board[innerIndex].Add(deck[currentCardIndex++]);//currentCardIndex at current value then increment it
                        }

                    }
                
                }


                //Console.WriteLine("[Checking in correct position]");

                //for (int index = 0; index < deck.Count; index++)
                //{
                //    //deck[index].InCorrectPlace = deck[index].Check(deck[index]);
                //
                //    deck[index].Print();
                //}

                String cardRun = "";
            

                int playerLives = 4;
                bool gameRunning = true;

                int runCount = 0;
                int cardsPickedUp = 0; //For each card picked up ++ and if not 52 by the end of the game, you lose

                while (gameRunning == true)
                {
                    //Console.WriteLine("Run #" + (runCount + 1));

                    cardRun += "\nRun #" + (runCount + 1) + "\n";

                    //take first card out of lives list (board[0])  //eg. first card is 4
                    Card currentCard = null;


                    //4th, 3rd, 2nd, 1st
                    if (playerLives >= 0)
                    {
                        //Assign the new card
                        currentCard = board[0][0];
                        //Add to the total cardsPickedUp
                        cardsPickedUp++;
                        //take the first card off board[0]
                        board[0].RemoveAt(0);              
                    }

                    while(true)
                    {
                    


                        if(currentCard.Value == 13)
                        {
                            cardRun += "." +  "K"/*currentCard.Value*/ + "\n";
                            playerLives--;
                            break;
                        }
                        else if (currentCard.Value == 12)
                        {
                            cardRun += "." + "Q"/*currentCard.Value*/;
                        }
                        else if (currentCard.Value == 11)
                        {
                            cardRun += "." + "J"/*currentCard.Value*/;
                        }
                        else
                        {
                            cardRun += "." + currentCard.Value;
                        }



                        //grab new card
                        int previousCardPile = currentCard.Value;
                        //Assign the new card
                        currentCard = board[currentCard.Value][0];
                        //Add to the total cardsPickedUp
                        cardsPickedUp++;
                        //remove the card from the board
                        board[previousCardPile].RemoveAt(0);


                    }

              
                    //Check lives before looping again
                    if (playerLives == 0)
                    {
                        gameRunning = false;
                    }

 


                    runCount++;
                }





                Console.WriteLine((cardsPickedUp == 52) ? "Congrats! You won!" : "Bad luck!");
                if (cardsPickedUp == 52)
                {
                    gameWinCounter++;
                }
                Console.WriteLine("Cards picked up: " + cardsPickedUp + "/52");
                Console.WriteLine(cardRun);

                String cardsRemaining = "";
                int remainingCardCount = 0;
                //For every list in board
                foreach (List<Card> outerCardList in board)
                {
                    //for every card in the board list
                    foreach (Card currentCard in outerCardList)
                    {
                        remainingCardCount++;
                        if (currentCard.Value == 13)
                        {
                            cardsRemaining += "." + "K"/*currentCard.Value*/;
                        }
                        else if (currentCard.Value == 12)
                        {
                            cardsRemaining += "." + "Q"/*currentCard.Value*/;
                        }
                        else if (currentCard.Value == 11)
                        {
                            cardsRemaining += "." + "J"/*currentCard.Value*/;
                        }
                        else
                        {
                            cardsRemaining += "." + currentCard.Value;
                        }
                    }
                }



                Console.WriteLine("Cards remaining: "+remainingCardCount+"\n" + cardsRemaining +"\n\n");

                //Console.Clear();
            }


            //Stats of the game that has been looped multiple times
            Console.WriteLine("\nTotal Wins: " + gameWinCounter +"/"+ gameLoopCounter + "\nWin rate: " + (float)gameWinCounter / gameLoopCounter + "%");

            Console.ReadLine();
        }


        public static class ThreadSafeRandom
        {
            [ThreadStatic]
            private static Random Local;

            public static Random ThisThreadsRandom
            {
                get { return Local ?? (Local = new Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId))); }
            }
        }
        public static void ShuffleDeck(List<Card> deck)
        {
            int currentSize = deck.Count;
            while (currentSize > 1)
            {
                currentSize--;
                int randomIndex = ThreadSafeRandom.ThisThreadsRandom.Next(currentSize + 1);
                int holder = deck[randomIndex].Value;
                deck[randomIndex].Value = deck[currentSize].Value;
                deck[currentSize].Value = holder;
            }
        }

    }//End of class
}
